﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Predictionist.Data.Sql;

namespace Predictionist.Data.Sql.Migrations
{
    [DbContext(typeof(SqlDbContext))]
    partial class SqlDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FixtureTeam", b =>
                {
                    b.Property<int>("FixturesId")
                        .HasColumnType("int");

                    b.Property<int>("TeamsId")
                        .HasColumnType("int");

                    b.HasKey("FixturesId", "TeamsId");

                    b.HasIndex("TeamsId");

                    b.ToTable("FixtureTeam");
                });

            modelBuilder.Entity("Predictionist.Models.Fixture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Stage")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<int>("TeamsLeft")
                        .HasColumnType("int")
                        .HasComment("Number of teams left in the tournament");

                    b.Property<string>("Title")
                        .HasMaxLength(10)
                        .HasColumnType("nvarchar(10)")
                        .HasComment("E.g., group name");

                    b.Property<int>("TournamentId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TournamentId");

                    b.ToTable("Fixtures");

                    b
                        .HasComment("Represents part of the tournament, like one group or knockout match");
                });

            modelBuilder.Entity("Predictionist.Models.FixtureLink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("FromFixtureId")
                        .HasColumnType("int");

                    b.Property<int>("Place")
                        .HasColumnType("int");

                    b.Property<int>("ToFixtureId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("FromFixtureId");

                    b.HasIndex("ToFixtureId");

                    b.ToTable("FixtureLinks");
                });

            modelBuilder.Entity("Predictionist.Models.FixtureResult", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("FixtureId")
                        .HasColumnType("int");

                    b.Property<int>("Place")
                        .HasColumnType("int");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("FixtureId");

                    b.HasIndex("TeamId");

                    b.ToTable("FixtureResults");
                });

            modelBuilder.Entity("Predictionist.Models.Match", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AwayTeamGoals")
                        .HasColumnType("int");

                    b.Property<int>("AwayTeamId")
                        .HasColumnType("int");

                    b.Property<int>("AwayTeamPenaltyGoals")
                        .HasColumnType("int");

                    b.Property<int?>("FixtureId")
                        .HasColumnType("int");

                    b.Property<int>("HomeTeamGoals")
                        .HasColumnType("int");

                    b.Property<int>("HomeTeamId")
                        .HasColumnType("int");

                    b.Property<int>("HomeTeamPenaltyGoals")
                        .HasColumnType("int");

                    b.Property<bool>("Overtime")
                        .HasColumnType("bit");

                    b.Property<bool>("Penalties")
                        .HasColumnType("bit");

                    b.Property<string>("Result")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(100)")
                        .HasDefaultValue("Unknown");

                    b.HasKey("Id");

                    b.HasIndex("AwayTeamId");

                    b.HasIndex("FixtureId");

                    b.HasIndex("HomeTeamId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("Predictionist.Models.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.HasKey("Id");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("Predictionist.Models.Tournament", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Tournaments");
                });

            modelBuilder.Entity("FixtureTeam", b =>
                {
                    b.HasOne("Predictionist.Models.Fixture", null)
                        .WithMany()
                        .HasForeignKey("FixturesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Predictionist.Models.Team", null)
                        .WithMany()
                        .HasForeignKey("TeamsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Predictionist.Models.Fixture", b =>
                {
                    b.HasOne("Predictionist.Models.Tournament", "Tournament")
                        .WithMany("Fixtures")
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Tournament");
                });

            modelBuilder.Entity("Predictionist.Models.FixtureLink", b =>
                {
                    b.HasOne("Predictionist.Models.Fixture", "FromFixture")
                        .WithMany("FromFixtureLinks")
                        .HasForeignKey("FromFixtureId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Predictionist.Models.Fixture", "ToFixture")
                        .WithMany("ToFixtureLinks")
                        .HasForeignKey("ToFixtureId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("FromFixture");

                    b.Navigation("ToFixture");
                });

            modelBuilder.Entity("Predictionist.Models.FixtureResult", b =>
                {
                    b.HasOne("Predictionist.Models.Fixture", "Fixture")
                        .WithMany("FixtureResults")
                        .HasForeignKey("FixtureId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Predictionist.Models.Team", "Team")
                        .WithMany("FixtureResults")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Fixture");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("Predictionist.Models.Match", b =>
                {
                    b.HasOne("Predictionist.Models.Team", "AwayTeam")
                        .WithMany("AwayMatches")
                        .HasForeignKey("AwayTeamId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Predictionist.Models.Fixture", "Fixture")
                        .WithMany("Matches")
                        .HasForeignKey("FixtureId");

                    b.HasOne("Predictionist.Models.Team", "HomeTeam")
                        .WithMany("HomeMatches")
                        .HasForeignKey("HomeTeamId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("AwayTeam");

                    b.Navigation("Fixture");

                    b.Navigation("HomeTeam");
                });

            modelBuilder.Entity("Predictionist.Models.Fixture", b =>
                {
                    b.Navigation("FixtureResults");

                    b.Navigation("FromFixtureLinks");

                    b.Navigation("Matches");

                    b.Navigation("ToFixtureLinks");
                });

            modelBuilder.Entity("Predictionist.Models.Team", b =>
                {
                    b.Navigation("AwayMatches");

                    b.Navigation("FixtureResults");

                    b.Navigation("HomeMatches");
                });

            modelBuilder.Entity("Predictionist.Models.Tournament", b =>
                {
                    b.Navigation("Fixtures");
                });
#pragma warning restore 612, 618
        }
    }
}
