﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Predictionist.Models
{
    [Comment("Represents part of the tournament, like one group or knockout match")]
    public class Fixture
    {
        public int Id { get; set; }

        [MaxLength(10)]
        [Comment("E.g., group name")]
        public string Title { get; set; }

        [Column(TypeName = "varchar(100)")]
        public TournamentStage Stage { get; set; }

        [Comment("Number of teams left in the tournament")]
        public int TeamsLeft { get; set; }

        public int TournamentId { get; set; }
        public Tournament Tournament { get; set; }
        public ICollection<Team> Teams { get; set; }
        public ICollection<Match> Matches { get; set; }
        public ICollection<FixtureResult> FixtureResults { get; set; }
        public ICollection<FixtureLink> FromFixtureLinks { get; set; }
        public ICollection<FixtureLink> ToFixtureLinks { get; set; }
    }

    public enum TournamentStage
    {
        Qualification,
        Group,
        Knockout
    }
}
