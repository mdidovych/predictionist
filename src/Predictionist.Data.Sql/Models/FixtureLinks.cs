﻿namespace Predictionist.Models
{
    public class FixtureLink
    {
        public int Id { get; set; }

        public int Place { get; set; }

        public int FromFixtureId { get; set; }
        public Fixture FromFixture { get; set; }

        public int ToFixtureId { get; set; }
        public Fixture ToFixture { get; set; }
    }
}
