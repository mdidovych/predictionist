﻿namespace Predictionist.Models
{
    public class FixtureResult
    {
        public int Id { get; set; }

        public int Place { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }

        public int FixtureId { get; set; }
        public Fixture Fixture { get; set; }
    }
}
