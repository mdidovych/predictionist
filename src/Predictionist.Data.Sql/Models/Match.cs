﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Predictionist.Models
{
    public class Match
    {
        public int Id { get; set; }

        [Column(TypeName = "varchar(100)")]
        public MatchResult Result { get; set; }

        public int HomeTeamGoals { get; set; }
        public int AwayTeamGoals { get; set; }

        public bool Overtime { get; set; }
        public bool Penalties { get; set; }

        public int HomeTeamPenaltyGoals { get; set; }
        public int AwayTeamPenaltyGoals { get; set; }

        public int HomeTeamId { get; set; }
        public Team HomeTeam { get; set; }
        public int AwayTeamId { get; set; }
        public Team AwayTeam { get; set; }
        public int? FixtureId { get; set; }
        public Fixture Fixture { get; set; }
    }

    public enum MatchResult
    {
        Unknown,
        HomeTeamWin,
        Draw,
        AwayTeamWin
    }
}
