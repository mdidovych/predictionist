﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Predictionist.Models
{
    public class Team
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public ICollection<Match> HomeMatches { get; set; }
        public ICollection<Match> AwayMatches { get; set; }
        public ICollection<Fixture> Fixtures { get; set; }
        public ICollection<FixtureResult> FixtureResults { get; set; }
    }
}
