﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Predictionist.Models
{
    public class Tournament
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public ICollection<Fixture> Fixtures { get; set; }
    }
}
