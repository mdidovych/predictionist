﻿using Microsoft.EntityFrameworkCore;
using Predictionist.Models;

namespace Predictionist.Data.Sql
{
    public class SqlDbContext : DbContext
    {
        public SqlDbContext(DbContextOptions<SqlDbContext> options)
            : base(options)
        {
        }

        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Fixture> Fixtures { get; set; }
        public DbSet<FixtureResult> FixtureResults { get; set; }
        public DbSet<FixtureLink> FixtureLinks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Match>()
                .Property(match => match.Result)
                .HasDefaultValue(MatchResult.Unknown);
            modelBuilder.Entity<Match>()
                .HasOne(match => match.HomeTeam)
                .WithMany(team => team.HomeMatches)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Match>()
                .HasOne(match => match.AwayTeam)
                .WithMany(team => team.AwayMatches)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<FixtureLink>()
                .HasOne(link => link.FromFixture)
                .WithMany(fixture => fixture.FromFixtureLinks)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<FixtureLink>()
                .HasOne(link => link.ToFixture)
                .WithMany(fixture => fixture.ToFixtureLinks)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
